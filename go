#!/usr/bin/env bash
set -euo pipefail

if [[ -z ${IMAGE_NAME:-} ]]; then
  IMAGE_NAME=coding-challenge-2
fi 

function help() {
  echo -e "Usage: go <command>"
  echo -e
  echo -e "    help               Print this help"
  echo -e "    run                Run locally without building binary"
  echo -e "    build-docker       Build docker image and run it"
  echo -e "    test               Run local unit tests"
  echo -e "    ci-test            Run CI unit tests"
  echo -e "    lint               Run various python linting modules through the codebase"
  echo -e "    init               Set up local virtual env"
  echo -e 
  exit 0
}

function init() {

  _console_msg "Initialising local virtual environment ..." INFO true

  pipenv install --dev

  _console_msg "Init complete" INFO true

}

function run() {

  _console_msg "Running python:main ..." INFO true

  pipenv run python3 main.py "${@:-}"

  _console_msg "Execution complete" INFO true

}

function test() {

    _console_msg "Running local unit tests ..." INFO true

    pipenv run pytest -s -v "./src/test_labels.py" 

    pipenv run pytest --cov

    _console_msg "Tests complete" INFO true

}

function ci-test() {

    _console_msg "Running CI unit tests ..." INFO true

    pip install pipenv==2018.10.13
    pipenv install --dev --deploy --ignore-pipfile --system
    pipenv run pytest -s -v "./src/test_labels.py" --disable-pytest-warnings --junit-xml junit-report.xml  
    pipenv lock -r > requirements.txt

    _console_msg "Tests complete" INFO true

}

function lint() {

    _console_msg "Running lint tests ..." INFO true

    python_files=$(find . -name '*.py')

    _console_msg "Running pylint ..."

    for file in ${python_files}; do
      pipenv run pylint ${file} --score=no --disable=C0111,C0301
    done

    _console_msg "Running flake8 ..."

    for file in ${python_files}; do
      pipenv run flake8 ${file} --ignore=E501,E123,W503
    done

    _console_msg "Running pycodestyle ..."
    
    for file in ${python_files}; do
      pipenv run pycodestyle ${file} --ignore=E501,E123,W503
    done

    _console_msg "Linting complete" INFO true

}

function build-docker() {

  _console_msg "Building python docker image ..." INFO true

  docker build -t ${IMAGE_NAME} .

  docker run ${IMAGE_NAME} "${1:-}"

  _console_msg "Build complete" INFO true

}

function _assert_variables_set() {

  local error=0
  local varname
  
  for varname in "$@"; do
    if [[ -z "${!varname-}" ]]; then
      echo "${varname} must be set" >&2
      error=1
    fi
  done
  
  if [[ ${error} = 1 ]]; then
    exit 1
  fi

}

function _console_msg() {

  local msg=${1}
  local level=${2:-}
  local ts=${3:-}

  if [[ -z ${level} ]]; then level=INFO; fi
  if [[ -n ${ts} ]]; then ts=" [$(date +"%Y-%m-%d %H:%M")]"; fi

  echo ""

  if [[ ${level} == "ERROR" ]] || [[ ${level} == "CRIT" ]] || [[ ${level} == "FATAL" ]]; then
    (echo 2>&1)
    (echo >&2 "-> [${level}]${ts} ${msg}")
  else 
    (echo "-> [${level}]${ts} ${msg}")
  fi

  echo ""

}

function ctrl_c() {
    if [ ! -z ${PID:-} ]; then
        kill ${PID}
    fi
    exit 1
}

trap ctrl_c INT

if [[ ${1:-} =~ ^(help|run|build-docker|test|ci-test|lint|init)$ ]]; then
  COMMAND=${1}
  shift
  $COMMAND "$@"
else
  help
  exit 1
fi
