#!/usr/bin/env python3

'''CLI wrapper for fix_price_label function'''

import sys
from src.labels import fix_price_label

try:
    print(fix_price_label(sys.argv[1]))
except IndexError:
    print('USAGE: ./main.py "<price label to convert>"\n')
    sys.exit(1)
