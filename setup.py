'''Package locator for pipenv'''

from setuptools import setup, find_packages

setup(
    name="labels",
    packages=find_packages(where='src'),
    package_dir={'': 'src'}
    )
