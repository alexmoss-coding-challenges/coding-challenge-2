'''correctly formats a label with adjusted pricing information'''

import re
from decimal import Decimal


def fix_price_label(invalid_label: str) -> str:

    output_prices = []

    # extract numbers with 0 or 1 decimal
    input_prices = [s for s in re.findall(r"\b\d+[\.]?\d*\b", invalid_label)]

    input_prices = [Decimal(x) for x in input_prices]

    # flip order from input as need to retain rightmost prices + lose dupes
    input_prices.reverse()

    # re-index the list for each successive max value found
    while input_prices:
        current_max_price = max(input_prices)
        # slice start->new max
        input_prices = input_prices[:input_prices.index(current_max_price)]
        output_prices.append(current_max_price)

    output_prices = [str(x) for x in output_prices]

    try:

        if len(output_prices) == 1:
            return 'Now £' + output_prices[0]

        return (
            'Was £'
            + ', then £'.join(output_prices[:len(output_prices) - 1])
            + ', now £' + output_prices[-1]
        )

    except IndexError:
        return ""
