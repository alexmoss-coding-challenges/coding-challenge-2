from labels import fix_price_label


# Tests from Specification itself

def test_spec_example_1():
    label = "Was £10, then £8.00, then £11, now £6"
    assert fix_price_label(label) == "Was £11, now £6"


def test_spec_example_2():
    label = "Was £10, then £8, then £8, now £6"
    assert fix_price_label(label) == "Was £10, then £8, now £6"


def test_spec_example_3():
    label = "Was £10, then £6, then £4, now £8"
    assert fix_price_label(label) == "Was £10, now £8"


# Test basic processing

def test_single_price():
    label = "£15.95"
    assert fix_price_label(label) == "Now £15.95"


def test_ascending_prices():
    label = "Was £10, then £12, then £14, then £16, then £18, then £20"
    assert fix_price_label(label) == "Now £20"


def test_descending_prices():
    label = "Was £20, then £18, then £16, then £14, then £12, then £10"
    assert fix_price_label(label) == "Was £20, then £18, then £16, then £14, then £12, now £10"


def test_multiple_middle_prices_descending():
    label = "Was £19, then £18, then £17, then £16, then £15"
    assert fix_price_label(label) == "Was £19, then £18, then £17, then £16, now £15"


def test_unchanging_prices():
    label = "Was £15.25, Then £15.25, Then £15.25, Now £15.25"
    assert fix_price_label(label) == "Now £15.25"


def test_skip_all_other_prices():
    label = "Was £10, then £11, then £8, now £20"
    assert fix_price_label(label) == "Now £20"


def test_skip_middle_prices():
    label = "Was £200, then £150.00, then £160, now £175.00"
    assert fix_price_label(label) == "Was £200, now £175.00"


# Test Duplicate handling

def test_then_equals_now_retain_then():
    label = "Was £20, then £17, then £18, now £17"
    assert fix_price_label(label) == "Was £20, then £18, now £17"


def test_then_equals_now_reject_then():
    label = "Was £20, then £17.50, now £17.50"
    assert fix_price_label(label) == "Was £20, now £17.50"


def test_then_equals_then():
    label = "Was £20, then £17.50, then £17.50, now £17"
    assert fix_price_label(label) == "Was £20, then £17.50, now £17"


def test_was_equals_then():
    label = "Was £20, then £17, then £20, now £16"
    assert fix_price_label(label) == "Was £20, now £16"


def test_was_equals_then_and_then_equals_now():
    label = "Was £20, then £20, then £15, now £15"
    assert fix_price_label(label) == "Was £20, now £15"


# Test complex processing

def test_multiple_middle_prices_some_discarded():
    label = "Was £19, then £17, then £21, then £20, then £16, then £15"
    assert fix_price_label(label) == "Was £21, then £20, then £16, now £15"


def test_confirm_left_to_right_float_second():
    label = "Was £18, then £17, then £18.00, now £11.50"
    assert fix_price_label(label) == "Was £18.00, now £11.50"


def test_confirm_left_to_right_float_first():
    label = "Was £18.00, then £17, then £18, now £11.50"
    assert fix_price_label(label) == "Was £18, now £11.50"


def test_reject_middle_price_below_other_middle_price():
    label = "Was £20, then £15, then £17.50, now £10"
    assert fix_price_label(label) == "Was £20, then £17.50, now £10"


def test_reject_middle_price_below_final_price():
    label = "Was £20, then £12, then £17.50, now £15.00"
    assert fix_price_label(label) == "Was £20, then £17.50, now £15.00"


# Test edge cases and unusual label

def test_irrelevant_strings():
    label = "Was £20, cheese £18, thn £17, now £15, now £12"
    assert fix_price_label(label) == "Was £20, then £18, then £17, then £15, now £12"


def test_big_numbers():
    label = "Was £100000000000000000000000000000000, then £900000000000000.99, now £800000"
    assert fix_price_label(label) == "Was £100000000000000000000000000000000, then £900000000000000.99, now £800000"


def test_just_a_number():
    label = "20"
    assert fix_price_label(label) == "Now £20"


# Leaving test in as validates error handling of no numbers
def test_invalid_string():
    label = "notapricelabel"
    assert fix_price_label(label) == ""
